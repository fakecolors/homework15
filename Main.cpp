#include <iostream>
#include <cmath>
#include <string>


//Homework15_Part 01

//int main()
//{
//	std::cout << "Input N = ";
//	int n = 0;
//	std::cin >> n;
//
//	//Print only even numbers
//	for (int i = 0; i <= n; ++i)
//	{
//		double a = i / 2.0;
//		if (floor(a) == a)
//		{
//			std::cout << i << std::endl;
//		}
//	}
//
//	return 0;
//}

void func(bool b, int n)
{
	std::cout << "Result : ";
	for (int i = 0 + b; i <= n; i += 2)
	{
		std::cout << i << " ";
	}
}


int main()
{
	std::cout << "Input N = ";
	int n;
	std::cin >> n;

	std::cout << "Print only odd numbers (y/n) : ";
	std::string letter;
	std::cin >> letter;

	bool value = false;
	if (letter == "y")
	{
		value = true;
	}

	func(value, n);

	return 0;
}